from sys import argv
script, user_name = argv
prompt = '> '
print ("Hi %s, I'm the %s script." % (user_name, script))
print ("I'd like to ask you a few questions.")
print( "Do you like me %s?" % user_name)
likes = input(prompt)
#likes1=input()
print ("Where do you live %s?" % user_name)
lives = input(prompt)
#lives1=input()
print ("What kind of computer do you have?")
computer = input(prompt)
#computer1=input()

print( """
Alright, so you said %r about liking me.
You live in %r. Not sure where that is.
And you have a %r computer. Nice.
""" % (likes, lives, computer))

#prompt is an optional parameter of input() function which prints the strored string whenever the input function is called